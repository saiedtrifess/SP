$(function() {
	$('#submit').click(function() {
		if (!$("#message").val()) {
			alert("This field can't left empty");
		} else {

			var post_data = {
				'message': $("#message").val(),
				'name': $("#name").text(),
				'<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
			};
			$.ajax({
				type: "POST",
				url: "insertByajax",
				data: post_data,
				success: function(message, name) {
					// console.log(message);
					comment_insert();
					$("#result").html(message);


				}
			});
		}
	});

	function comment_insert() {
		var t = '';
		t += '<div class="panel panel-default">'
		t += '<div class="panel-body">';
		t += '<div class="pull-left">';
		t += '<a href="#">';
		t += '<img class="media-object img-circle" src="/assets/img/student.png" width="50px" height="50px" style="margin-right:8px; margin-top:-5px;">';
		t += '</a>';
		t += '</div>';
		t += '<h4 ><a href="#" style="text-decoration:none;"><p id="usernameout"> <strong></strong></p>  </a> – <small><small><a href="#" style="text-decoration:none; color:grey;"><i><i class="fa fa-clock-o" aria-hidden="true"></i> </i></a></small></small></h4>';

		t += '<span>';
		t += '<div class="navbar-right">';
		t += '<div class="dropdown">';
		t += '<button class="btn btn-link btn-xs dropdown-toggle" type="button" id="dd1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">';
		t += '<i class="fa fa-cog" aria-hidden="true"></i>';
		t += '<span class="caret"></span>';
		t += '</button>';
		t += '<ul class="dropdown-menu" aria-labelledby="dd1" style="float: right;">';
		t += '<li><a href="#"><i class="fa fa-fw fa-exclamation-triangle" aria-hidden="true"></i> Report</a></li>';
		t += '<li >';
		t += '<li><a href="#"><i class="fa fa-fw fa-bell" aria-hidden="true"></i> Enable notifications for this post</a></li>'
		t += '<li><a href="#"><i class="fa fa-fw fa-eye-slash" aria-hidden="true"></i> Hide</a></li>';
		t += '<li role="separator" class="divider"></li>';
		t += '<li><a href="#"><i class="fa fa-fw fa-trash" aria-hidden="true"></i> Delete</a></li>';
		t += '</ul>';
		t += '</div>';
		t += '</div>';
		t += '</span>';
		t += '<hr>';
		t += '<div id="<?php echo $row->id; ?>" class="post-content">';
		t += '<p id="result"><?php echo $row->message; ?></p>';
		t += '</div>';


		t += '<div>';

		t += '<br>';
		t += '</div>';

		t += '<div class="media">';
		t += '<div class="pull-left">';
		t += '<div>';

		t += '</div>';


		t += '<div class="media-body">';
		t += '<form action="">';
		t += '<button  id="answer" class="btn btn-danger ">Answer The question</button>';
		t += '</form>';
		t += '</div>'
		t += '</div>';
		t += '</div>'
		t += '</div>';
		t += '<br><br>';
		t += '</div>';

		$('.comments-holder-ul').append(t);
	}

});


