<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Welcome to student profile</title>
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/SP/SP/assets/css/welcome.css">
        <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="/assets/js/button.js"></script>
        <script src="/assets/js/other.js"></script>
        <script src="/assets/js/other2.js"></script>
        
    </head>
    <body class="rowset">
        <div class="row">
            <div class="col-md-2">
                <img src="/SP/SP/assets/img/logo.png" class="img-rounded" alt="Cinque Terre" width="100" height="50">
                
            </div>
            <div class="col-md-10 fcol">
                <ul class="nav nav-tabs">
                    <li role="presentation" ><a href="<?php echo ('/SP/SP/index.php');?>"><p class="headfont"><b>Home</b></p></a></li>
                    
                </ul>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="container">
                        <h2 class="login">Student Login</h2>
                        <?php
                        if(!empty($success_msg)){
                        echo '<p class="statusMsg">'.$success_msg.'</p>';
                        }elseif(!empty($error_msg)){
                        echo '<p class="statusMsg">'.$error_msg.'</p>';
                        }
                        ?>
                        <form action="<?php echo ('login') ;?>" method="post">
                            <div class="form-group has-feedback">
                                <input type="email" class="form-control" name="email" placeholder="Email" required="" value="">
                                <?php echo form_error('email','<span class="help-block">','</span>'); ?>
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="password" placeholder="Password" required="">
                                <?php echo form_error('password','<span class="help-block">','</span>'); ?>
                            </div>
                            <div class="form-group">
                                <input type="submit" name="loginSubmit" class="btn-primary" value="Submit"/>
                            </div>
                        </form>
                        <p class="footInfo">Don't have an account? <a href="<?php echo ('registration');?>">Register here</a></p>
                    </div>
                </div>
            </body>
        </html>