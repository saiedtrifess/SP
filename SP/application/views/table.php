<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css">
<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="/assets/js/jquery.mobile-1.4.5.min.js"></script>
<style>
th {
    border-bottom: 1px solid #d6d6d6;
}

tr:nth-child(even) {
    background: #e9e9e9;
}
</style>

</head>
<body>

<div data-role="page" id="pageone">
  <div data-role="header">
    <h1>List of the student and what they can offer you</h1>
  </div>
    <div class="row">
<div class="col-md-12 fcol">
<ul class="nav nav-tabs">
  <li role="presentation" onclick="goBack()"><a href="#"><b><p class="headfont">Back</p></b></a></li>
  </ul>
</div>
</div>

  
  <div data-role="main" class="ui-content">
    <form>
      <input id="filterTable-input" data-type="search" placeholder="Search For...">
    </form>
    <table data-role="table" data-mode="columntoggle" class="ui-responsive ui-shadow" id="myTable" data-filter="true" data-input="#filterTable-input">
      <thead>
        <tr>
          <th data-priority="6">Name</th>
          <th data-priority="1">Study level</th>
          <th data-priority="2">email</th>
          <th data-priority="3">Major</th>
          <th data-priority="5">Skype</th>
          <th data-priority="5">Title</th>
          <th data-priority="5">What he / she can do for you</th>
          <th data-priority="5">Country </th>
        </tr>
      </thead>
      <tbody>
      <?php foreach ($query as $row  ): ?>
        <tr>
    <td><?php echo $row->name; ?></td>
    <td><?php echo $row->iam; ?></td>
    <td><a href ="mailto:<?php echo $row->email; ?>">Contact Student</a></td>
    <td><?php echo $row->studnetof; ?></td>
    <td><?php echo $row->skype; ?></td>
    <td><?php echo $row->ican; ?></td>
    <td><?php echo $row->describe; ?></td>
    <td><?php echo $row->country; ?></td>
     </tr>
<?php endforeach; ?>

      </tbody>
    </table>
  </div>


 <footer class="navbar-default ">
<div class="row">
   <div class="container-fluid1">
   <div class="col-md-4">
      <span> Welcome to sudentprofile.eu</span>
      </div>
   <div class="col-md-6">
      <span> Saiedtrifess@gmail.com</span><br>
      <span> Riga,Latvia</span><br>
      <span>Brivabs,171</span>
      <span> Number : 0037128949227</span>
   </div>
   <div class="col-md-2">
    <img src="/assets/img/logo.png" class="img-rounded" alt="Cinque Terre" width="100" height="70">
   </div>
   </div>
</footer>
<script type="text/javascript">
function goBack() {
    window.history.back();
}
</script>

</body>
</html>

