<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/assets/css/abroad.css">
    <title>Study abroad</title>
  </head>
  <body>
    <div class="row">
      <div class="col-md-2">
        <a href="<?php echo ('/');?>"> <img src="/assets/img/logo.png" class="img-rounded" alt="Cinque Terre" width="100" height="65"></a>
        
      </div>
      <div class="col-md-10 fcol">
        <ul class="nav nav-tabs">
          <li role="presentation" ><a href="<?php echo ('/');?>"><p class="headfont"><b>Home</b></p></a></li>
        </ul>
      </div>
    </div>
    <div class="jumbotron">
      <div class="container">
        <h1 class="display-3">StudnetProfile</h1>
       <p> Student Profile is the First Academic Social Network Created by group of students in order to ease the challenges the 21st Century Students are facing, Our support team is available to help 24/7 in <b>Arabic, English and French</b></p>

        
      </div>
    </div>
    <div class="container">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-md-4">
          <h2>Admissions:</h2>
          <p>StudentProfile will help you to get a university admission in accredited universities in the European Union that are recognised worldwide. </p>
          <p><a class="btn btn-secondary" href="<?php echo base_url('index.php/testo') ;?>" role="button">View details &raquo;</a></p>
        </div>
        <div class="col-md-4">
          <h2>Trainings:</h2>
          <p>StudentProfile will help you to get the training you wish to have in order to enhance your previously gained knowledge. </p>
          <p><a href="/assets/pdf/studentprofiletranning.pdf" download>Download traning information &raquo;</a></p>
        </div>
        <div class="col-md-4">
          <h2>Tutors:</h2>
          <p>StudentProfile will help you to get a tutor that will help you overcome any obstacle you are facing, home works, languages, etc. you ask and others will be there to help.</p>
          <p><a class="btn btn-secondary" href="<?php echo base_url('index.php/registration') ;?>" role="button">Regisiter here &raquo;</a></p>
        </div>
      </div>
      <hr>
      <footer>
        <p>&copy; Company 2017</p>
      </footer>
      </div> <!-- /container -->
      <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
      <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
      <script src="../../dist/js/bootstrap.min.js"></script>
      <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
      <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
    </body>
  </html>