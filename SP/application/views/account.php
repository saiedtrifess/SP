<!DOCTYPE html>
<html>
    <head>
        <title>Profile</title>
        <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://bootswatch.com/cosmo/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/profile.css">
        <script src="/assets/js/profile.js"></script>
    </head>
    <body>
        <div class="mainbody container-fluid">
            <div class="row">
                <div class="navbar-wrapper">
                    <div class="container-fluid">
                        <div class="navbar navbar-default navbar-static-top" role="navigation">
                            <div class="container-fluid">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span><span
                                    class="icon-bar"></span><span class="icon-bar"></span>
                                    </button>
                                    <a class="navbar-brand" href="./ORqmj" style="margin-right:-8px; margin-top:-5px;">
                                        <img alt="Brand" src="/assets/img/logo.png" width="40px" height="40px">
                                    </a>
                                    <a class="navbar-brand" href="">StudentProfile</a>
                                    
                                </div>
                                <div class="navbar-collapse collapse">
                                    <ul class="nav navbar-nav">
                                        <li><a href="#">StudentProfile Services</a></li>
                                    </ul>
                                    <ul class="nav navbar-nav navbar-right">
                                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <span class="user-avatar pull-left" style="margin-right:8px; margin-top:-5px;">
                                                <img src="/assets/img/student.png" class="img-responsive img-circle" title="John Doe" alt="John Doe" width="30px" height="30px">
                                            </span>
                                            <span class="user-name">
                                                <?php echo $user['name']; ?>
                                            </span>
                                        <b class="caret"></b></a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <div class="navbar-content">
                                                    <div class="row">
                                                        <div class="col-md-5">
                                                            <img src="/assets/img/student.png" alt="Alternate Text" class="img-responsive" width="120px" height="120px" />
                                                            <p class="text-center small"></p>
                                                            
                                                        </div>
                                                        <div class="col-md-7">
                                                            <span id="username"><?php echo $user['name']; ?></span>
                                                            <p class="text-muted small">
                                                            <?php echo $user['email']; ?></p>
                                                            <div class="divider">
                                                            </div>
                                                            
                                                            <a href="#" class="btn btn-default btn-xs"><i class="fa fa-question-circle-o" aria-hidden="true"></i> Help!</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="navbar-footer">
                                                    <div class="navbar-footer-content">
                                                        <div class="row">
                                                            
                                                            <div class="col-md-6">
                                                                <a href="<?php echo ('logout') ;?>" class="btn btn-default btn-sm pull-right"><i class="fa fa-power-off" aria-hidden="true"></i> Sign Out</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="padding-top:50px;"> </div>
            <div class="col-lg-3 col-md-3 hidden-sm hidden-xs">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="media">
                            <div align="center">
                                <img class="thumbnail img-responsive" src="/assets/img/student.png" width="300px" height="300px">
                            </div>
                            <div class="media-body">
                                <hr>
                                <h3><strong>About me</strong></h3>
                                <p><?php echo $user['service']; ?></p>
                                <hr>
                                <h3><strong>Location</strong></h3>
                                <p><?php echo $user['country']; ?></p>
                                <hr>
                                <h3><strong>Specialization</strong></h3>
                                <p><?php echo $user['major']; ?></p>
                                <hr>
                                <h3><strong>Eduncation level</strong></h3>
                                <p><?php echo $user['educationlevel']; ?></p>
                                <h3><strong>Language</strong></h3>
                                <p><?php echo $user['lang']; ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>