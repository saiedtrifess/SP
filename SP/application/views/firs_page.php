<!DOCTYPE html>
<html>
  <head>
    <title>Welcome to Student Profile</title>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/SP/SP/assets/css/firs_page.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body class="col1">

    <!-- Firs row with back ground -->
    <div class="row">
      <div class="col-md-12">
        <ul class="nav nav-tabs">
          <li href="<?php echo ('/');?>"> <img src="/SP/SP/assets/img/logo.png"   width="120" height="42"></li>
          <li role="presentation" class="headPage"><a href="index.php/con">Contact Us</a></li>
          <li role="presentation" class="headPage"><a href="index.php/login">Sing In</a></li>
          <li role="presentation" class="headPage"><a href="index.php/registration">Register</a></li>
        </ul>
        <p class="text-danger"><b>StudentProfile: your Academic Social Network</b></p>
        <br><br><br><br><br><br><br><br><br><br><br><br>
      </div>
    </div>
    <!--Firs row End -->

    <!-- Second row with back ground -->
    
      <div class="jumbotron">
        <div class="container">
          <h1 class="display-3"><b>Student Profile</b></h1>
          <p class="intro"> <b>Student Profile is the First Academic Social Network Created by group of students in order to ease the challenges the 21st Century Students are facing, Our support team is available to help 24/7 in Arabic, English and French</b></p>
        </div>
      </div>
       <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
      
       <!-- Second End-->

     

      <!-- The panel-->
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <h2 class="blog" >Admissions:</h2>
            <p class="blog">StudentProfile will help you to get a university admission in accredited universities in the European Union that are recognised worldwide. </p>
            <p><b><a class="btn btn-secondary" href="<?php echo base_url('/testo') ;?>" role="button">View details &raquo;</a></b></p>
          </div>
          <div class="col-md-4">
            <h2 class="blog" >Trainings:</h2>
            <p class="blog">StudentProfile will help you to get the training you wish to have in order to enhance your previously gained knowledge. </p>
            <p><a href="/SP/SP/assets/pdf/studentprofiletranning.pdf" download>Download traning information &raquo;</a></p>
          </div>

          <div class="col-md-4">
            <h2 class="blog" >Tutors:</h2>
            <p class="blog" >StudentProfile will help you to get a tutor that will help you overcome any obstacle you are facing, home works, languages, etc. you ask and others will be there to help.</p>
            <p><a class="btn btn-secondary" href="<?php echo base_url('/registration') ;?>" role="button">Regisiter here &raquo;</a></p>
          </div>
        </div>
        <hr>
      </div>

      <!-- The footer of the page TBDL -->
      <footer class="footer">
        
          <div class="row">
              <div class="col-md-3">
                <h1 class="text-white">StudentProfile.</h1>
              </div>

              <div class="col-md-9">
                <div class="d-inline-block">
                  <div class="bg-circle-outline d-inline-block">
                  <a href="https://www.facebook.com/StudentProfile-1066050063525355/" class="text-white">
                    <i class="fa fa-2x fa-fw fa-facebook"></i>
                    </a>
                  </div>
                  <div class="bg-circle-outline d-inline-block">
                    <a href="https://twitter.com/StudentsProfile" class="text-white">
                    <i class="fa fa-2x fa-fw fa-twitter"></i>
                    </a>
                  </div>
                  <div class="bg-circle-outline d-inline-block">
                    <a href="https://www.linkedin.com/company/" class="text-white">
                    <i class="fa fa-2x fa-fw fa-linkedin"></i></a>
                  </div>
                </div>
              </div>    
          
        </div>
      </footer>
      </body>
  </html>