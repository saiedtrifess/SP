<!DOCTYPE html>
<html>
	<head>
		<title>Answers</title>
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		<href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<link rel="stylesheet" type="text/css" href="/assets/css/firs_page.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<script type="text/javascript" src="/assets/js/ans.js"> </script>
		<script type="text/javascript" src="/assets/js/reload.js"> </script>
	</head>
	<body>
		<p name="name" class="name" id="name"><strong><?php echo $user['name']; ?></strong></p>
		
		<?php
		
		foreach ($res as  $row):?>
		
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="pull-left">
					<a href="#">
						<img class="media-object img-circle" src="/assets/img/student.png" width="50px" height="50px" style="margin-right:8px; margin-top:-5px;">
					</a>
				</div>
				<h4 ><a href="#" style="text-decoration:none;"><p id="usernameout"> <strong></strong></p>  </a> – <small><small><a href="#" style="text-decoration:none; color:grey;"><i><i class="fa fa-clock-o" aria-hidden="true"></i> 42 minutes ago</i></a></small></small></h4>
				
				<span>
					<div class="navbar-right">
						<div class="dropdown">
							<button class="btn btn-link btn-xs dropdown-toggle" type="button" id="dd1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							<i class="fa fa-cog" aria-hidden="true"></i>
							<span class="caret"></span>
							</button>
							<ul class="dropdown-menu" aria-labelledby="dd1" style="float: right;">
								<li><a href="#"><i class="fa fa-fw fa-exclamation-triangle" aria-hidden="true"></i> Report</a></li>
								<li >
									<li><a href="#"><i class="fa fa-fw fa-bell" aria-hidden="true"></i> Enable notifications for this post</a></li>
									<li><a href="#"><i class="fa fa-fw fa-eye-slash" aria-hidden="true"></i> Hide</a></li>
									<li role="separator" class="divider"></li>
									<li><a href="#"><i class="fa fa-fw fa-trash" aria-hidden="true"></i> Delete</a></li>
								</ul>
							</div>
						</div>
					</span>
					<hr>
					<div class="post-content">
						<p id="result"><?php echo $row->message; ?></p>
					</div>
					
					
					<div>
						
						<br>
					</div>
					
					
				</div>
				<br><br>
				<?php endforeach; ?>
			</div>
			
			
			<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
				<div class="panel panel-default">
					
					<form>
						<div class="panel-body">
							<span>
								
								
								<textarea  name="message" id="message" placeholder="Answer The question" style="min-width: 100%" class="form-control" required></textarea>
								
							</span>
							<span class="pull-left">
								<button type="submit" id="check" class="btn btn-primary btn-xs" >Check Answers</button>
								<button type="submit" id="submit" class="btn btn-primary btn-xs" >Post</button>
							</span>
							<span class="pull-right">
								<a href="#" class="btn btn-link" style="text-decoration:none;"><i class="fa fa-lg fa-at" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Mention"></i></a>
								<a href="#" class="btn btn-link" style="text-decoration:none;"><i class="fa fa-lg fa-envelope-o" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Message"></i></a>
								<a href="#" class="btn btn-link" style="text-decoration:none;"><i class="fa fa-lg fa-ban" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Ignore"></i></a>
							</span>
						</div>
					</form>
				</div>
				<div class="comments-list">
					<ul class="comments-holder-ul">
						<?php
							
						foreach ($ans as  $row):?>
						
						<div id="postcomment" class="panel panel-default">
							<div class="panel-body">
								<div class="pull-left">
									<a href="#">
										<img class="media-object img-circle" src="/assets/img/student.png" width="50px" height="50px" style="margin-right:8px; margin-top:-5px;">
									</a>
								</div>
								<div>
									<p name="name" class="name" id="name"><strong>Student Answers</strong></p>
								</div>
								<h4 ><a href="#" style="text-decoration:none;"><p id="usernameout"> <strong></strong></p>  </a> – <small><small><a href="#" style="text-decoration:none; color:grey;"><i><i class="fa fa-clock-o" aria-hidden="true"></i> 42 minutes ago</i></a></small></small></h4>
								
								<span>
									<div class="navbar-right">
										<div class="dropdown">
											<button class="btn btn-link btn-xs dropdown-toggle" type="button" id="dd1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
											<i class="fa fa-cog" aria-hidden="true"></i>
											<span class="caret"></span>
											</button>
											<ul class="dropdown-menu" aria-labelledby="dd1" style="float: right;">
												<li><a href="#"><i class="fa fa-fw fa-exclamation-triangle" aria-hidden="true"></i> Report</a></li>
												<li >
													<li><a href="#"><i class="fa fa-fw fa-bell" aria-hidden="true"></i> Enable notifications for this post</a></li>
													<li><a href="#"><i class="fa fa-fw fa-eye-slash" aria-hidden="true"></i> Hide</a></li>
													<li role="separator" class="divider"></li>
													<li><a href="#"><i class="fa fa-fw fa-trash" aria-hidden="true"></i> Delete</a></li>
												</ul>
											</div>
										</div>
									</span>
									<hr>
									<div class="post-content">
										<p ><?php echo $row->answer; ?></p>
									</div>
									
									
									<div>
										
										<br>
									</div>
									
									
								</div>
								<br><br>
							</div>
							<?php endforeach; ?>
						</ul>
					</div>
				</div>
				<script type="text/javascript" src="/assets/js/answer.js"> </script>
			</body>
		</html>