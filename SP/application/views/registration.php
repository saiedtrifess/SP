<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Welcome to student profile</title>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/SP/SP/assets/css/welcome.css">
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="/SP/SP/assets/js/button.js"></script>
    <script src="/SP/SP/assets/js/other.js"></script>
    <script src="/SP/SP/assets/js/other2.js"></script>
  </head>
  <body class="rowset">
    <!-- Header  -->
    <div class="row">
      <div class="col-md-10 fcol">
        <ul class="nav nav-tabs">
          <li role="presentation" ><a href="<?php echo ('/SP/SP/index.php');?>"><p class="headfont"><b>Home</b></p></a></li>
          <li role="presentation" ><a href="<?php echo ('/');?>"><p class="headfont"><b>Contact US</b></p></a></li>
          <li role="presentation" ><a href="<?php echo ('/');?>"><p class="headfont"><b>Sign In</b></p></a></li>
          
        </ul>
      </div>
    </div>
    <!-- end  Header  -->
    <!-- Thanks you message -->
    <div class"row">
      <h3 class="text-center">Thank you for joing</h3>
    </div>
    <!-- end Thank you -->
    
    <!-- Registeraion form -->
    <div class="row ">
      <div class="container">
        
        <div class="col-md-8 ">
          <div class="container">
            <h2>Student Registration</h2>
            <form action="<?php echo ('registration') ;?>" method="post">
              
              <div class="form-group">
                <input type="text" id="name" name="name"  placeholder="Please enter your Full Name" class="form-control" required="">
              </div>
              <div class="form-group" >
                <input id="email1" type="email" class="form-control" name="email" placeholder="Email" required="" value="<?php echo !empty($user['email'])?$user['email']:''; ?>">
                <?php echo form_error('email','<span class="help-block">','</span>'); ?>
              </div>

            
              <div class="form-group">
                <input id="passwod1" type="password" class="form-control" name="password" placeholder="Password" required="">
                <?php echo form_error('password','<span class="help-block">','</span>'); ?>
              </div>
              <div class="form-group">
                <input id="passwod2" type="password" class="form-control" name="conf_password" placeholder="Please confirm your password" required="">
                <?php echo form_error('conf_password','<span class="help-block">','</span>'); ?>
              </div>
              
              
              <div class="form-group">
                <input id="sendinfo" type="submit" name="newUser" class="btn-primary" value="Submit"/>
              </div>
            </form>
            <p class="footInfo">Already have an account? <a href="<?php echo base_url('Login'); ?>">Login here</a></p>
          </div>
        </div>
      </div>
    </div>
    <!-- end Registration form -->
  </body>
</html>